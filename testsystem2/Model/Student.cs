﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testsystem2.Model
{
    public class Student
    {
       public string Name { get; set; }
        public string RollNumber { get; set; }
        public int Class { get; set; }
        public int Gender { get; set; }
        public string Password { get; set; }
        
    }
}
