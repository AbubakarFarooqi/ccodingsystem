﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Windows.Shapes;
using CliWrap;
using System.IO;
using CliWrap.Buffered;
using System.Threading;
using WindowsFormsApp2;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

namespace testsystem2
{








































    /// <summary>
    /// Interaction logic for TestForm.xaml
    /// </summary>
    /// 

    public partial class TestForm : Window
    {
        List<Model.Question> lst;
        Dictionary<int, bool> doneQuestion;
        string stId;
        int totalQuestions;
        int currentQuestion;
        int submittedQuestion;
        int minute;
        int second;
        System.Windows.Threading.DispatcherTimer dispatcherTimer;

        public void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            second--;
            if(minute == 0)
            {
                if(second%2==0)
                this.timer.Height += 5;
                else
                this.timer.Height -= 5;
                this.timer.Foreground = new SolidColorBrush(Colors.Red); 
            }
            if (second == 0 && minute ==0)
            {
                for(int i = 0; i < lst.Count; i++)
                {
                    if (!doneQuestion.ContainsKey(lst[i].Id))
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("declare @answer varchar(1000); set @answer = REPLACE(@code, '`', CHAR(10)); insert into SubmittedQuestion values(@questionID,@sessionID,@studentID,@answer) ", con);
                        cmd.Parameters.AddWithValue("@questionID", lst[i].Id);
                        cmd.Parameters.AddWithValue("@sessionID", lst[i].sessId);
                        cmd.Parameters.AddWithValue("@studentID", stId);
                        cmd.Parameters.AddWithValue("@code", "Time Up");
                        cmd.ExecuteNonQuery();
                    }
                }
                MessageBox.Show("Session End","Information",MessageBoxButton.OK,MessageBoxImage.Information);
                this.Hide();
                var form = new formLast();
                form.Closed += (s, args) => this.Close();
                form.Show();
            }

            if (second == 0)
            {
                minute--;
                second = 60;
            }
                this.timer.Content = minute + ":" + second;
        }
        public TestForm(List<Model.Question> lst, string stId)
        {
            InitializeComponent();
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();

            doneQuestion = new Dictionary<int, bool>();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select timer from Session where Id = @id",con);
            cmd.Parameters.AddWithValue("@id", lst[0].sessId);
            minute = int.Parse(cmd.ExecuteScalar().ToString());
            second = 1;
            this.timer.Content = minute + ":"+second;

            this.lst = lst;
            currentQuestion = 0;
            submittedQuestion = 0;
            totalQuestions = lst.Count;
            lblQNumber.Text = (currentQuestion + 1) + "/" + totalQuestions;
            txtQuestion.Text = lst[currentQuestion].questionText;
            txtOutput.Text = lst[currentQuestion].sampleOutput;
            this.stId = stId;

        }

        void OnClickForward(object sender, RoutedEventArgs e)
        {
            if (!(currentQuestion + 1 > totalQuestions))
            {

                for (int i = currentQuestion + 1; i < lst.Count; i++)
                {
                    if (!doneQuestion.ContainsKey(lst[i].Id))
                    {
                        currentQuestion = i;
                        break;
                    }
                }
                lblQNumber.Text = (currentQuestion + 1) + "/" + totalQuestions;
                txtQuestion.Text = lst[currentQuestion].questionText;
                txtOutput.Text = lst[currentQuestion].sampleOutput;
                txtCode.Text = "";
            }
        }

        void OnClickBackward(object sender, RoutedEventArgs e)
        {
            if (!(currentQuestion - 1 < 0))
            {

                for (int i = currentQuestion - 1; i >= 0; i--)
                {
                    if (!doneQuestion.ContainsKey(lst[i].Id))
                    {
                        currentQuestion = i;
                        break;
                    }
                }
                lblQNumber.Text = (currentQuestion + 1) + "/" + totalQuestions;
                txtQuestion.Text = lst[currentQuestion].questionText;
                txtOutput.Text = lst[currentQuestion].sampleOutput;
                txtCode.Text = "";
            }
        }
        private async  void  btnTest_Click(object sender, RoutedEventArgs e)
        {
            
            if(txtCode.Text.Length != 0)
            {
                StreamWriter writer = new StreamWriter("C:\\1\\1.c");
                writer.WriteLine(txtCode.Text);
                writer.Flush();
                writer.Close();
                /* ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = "cmd.exe";
                 psi.WindowStyle = ProcessWindowStyle.Normal;
                 psi.Arguments = @"/k gcc 1.c";
                 psi.RedirectStandardOutput = true;
                 psi.UseShellExecute = false;
                 Process p = new Process();
                 p.StartInfo = psi;
                 p.Start();
                 p.WaitForExit(3000);
                 txtCode.Text = p.StandardOutput.ReadToEnd();*/
                var err = new StringBuilder();
                try
                {
                    var r = await Cli.Wrap("gcc").WithArguments(new[] { "C:\\1\\1.c" }).WithStandardErrorPipe(PipeTarget.ToStringBuilder(err)).ExecuteBufferedAsync();
                    MessageBox.Show("No Syntax Error", "Test", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                catch (Exception ee)
                {
                    MessageBox.Show(err.ToString(),"Syntax Errors",MessageBoxButton.OK,MessageBoxImage.Error);
                }

                // if no output it means no error
                // if there is any ooutput it means some error
            }

        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            submittedQuestion++;
            
            doneQuestion[lst[currentQuestion].Id] = true;
            if(submittedQuestion +1 > totalQuestions)
            {
                MessageBox.Show("session end","Information",MessageBoxButton.OK, MessageBoxImage.Information);
                this.Hide();
                var form = new formLast();
                form.Closed += (s, args) => this.Close();
                form.Show();
            }
            else
            {
                string code = txtCode.Text;
                string[] codeWithLineBreaks = code.Split('\n');
                string finalCode = "";
                foreach(var i in codeWithLineBreaks)
                {
                    finalCode = finalCode + i + "`";
                }
                var con = Configuration.getInstance().getConnection();
                try
                {
                    SqlCommand cmd = new SqlCommand("declare @answer varchar(1000); set @answer = REPLACE(@code, '`', CHAR(10)); insert into SubmittedQuestion values(@questionID,@sessionID,@studentID,@answer) ", con);
                    cmd.Parameters.AddWithValue("@questionID", lst[currentQuestion].Id);
                    cmd.Parameters.AddWithValue("@sessionID", lst[currentQuestion].sessId);
                    cmd.Parameters.AddWithValue("@studentID", stId);
                    cmd.Parameters.AddWithValue("@code", finalCode);
                    cmd.ExecuteNonQuery();
                    for(int i = 0; i< lst.Count;i++)
                    {
                        if(!doneQuestion.ContainsKey(lst[i].Id))
                        {
                            currentQuestion = i;
                            break;
                        }
                    }
                    lblQNumber.Text = (currentQuestion + 1) + "/" + totalQuestions;
                    txtQuestion.Text = lst[currentQuestion].questionText;
                    txtOutput.Text = lst[currentQuestion].sampleOutput;
                    txtCode.Text = "";
                }
                catch (Exception ee)
                {
                    MessageBox.Show("Something went Wrong", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                }


            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void txtCode_TextChanged(object sender, EventArgs e)
        {
           /* if (txtCode.Document.Text[txtCode.CaretOffset-1] == '<')
            {
                for(int i =)
            }
                txtCode.Document.Text += ">";
            if (txtCode.Document.Text[txtCode.Text.Length-1] == '(')
                txtCode.Document.Text += ")";
            if (txtCode.Text[txtCode.Text.Length-1] == '[')
                txtCode.Document.Text += "]";
            if (txtCode.Text[txtCode.Text.Length - 1] == '{')
                txtCode.Document.Text += "\n}";*/

        }
    }
}
