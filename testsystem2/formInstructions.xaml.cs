﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using testsystem2.Model;
using WindowsFormsApp2;

namespace testsystem2
{
    /// <summary>
    /// Interaction logic for formInstructions.xaml
    /// </summary>
    public partial class formInstructions : Window
    {
        Model.Student student;
        int sessionId;
        public formInstructions(Model.Student student , int sessionId )
        {
            InitializeComponent();
            this.student = student;
            this.sessionId = sessionId;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Question where SessionID = @sid",con);
            cmd.Parameters.AddWithValue("@sid", sessionId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            List<Model.Question> lst = new List<Model.Question>();
            for(int i = 0; i< dt.Rows.Count; i++)
            {
                Question q = new Question { Id = int.Parse(dt.Rows[i]["ID"].ToString()), questionText = dt.Rows[i]["QuestionText"].ToString(),
                    sampleOutput = dt.Rows[i]["SampleOutput"].ToString(), sessId = int.Parse(dt.Rows[i]["SessionID"].ToString())};

                lst.Add(q);
            }

            TestForm tf = new TestForm(lst,student.RollNumber);
            this.Close();
            tf.Show();




            
        }
    }
}
