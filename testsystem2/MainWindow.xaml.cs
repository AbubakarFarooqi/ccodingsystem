﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WindowsFormsApp2;

namespace testsystem2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int sessId;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
       


        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select SessionID from AvailableSession where isAvailable = 1",con);
            object sessionId =  cmd.ExecuteScalar();
            if(sessionId != null)
            {
               // pnlNotAvailable.Visibility = Visibility.Hidden;
                sessId = int.Parse(sessionId.ToString());

            }
            else
            {
                pnlAvailable.Visibility = Visibility.Hidden;
                MessageBox.Show("No Available Session","Information",MessageBoxButton.OK,MessageBoxImage.Information);

            }
        }

        private void txtRoll_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtPassword_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select * from [dbo].[Student] where [RollNumber] = @roll AND [Password] = @pass", con);
            cmd.Parameters.AddWithValue("@roll", txtRoll.Text);
            cmd.Parameters.AddWithValue("@pass", txtPassword.Text.ToString());
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Model.Student st = new Model.Student();
            if (dt.Rows.Count == 0)
            {
                /*   st.Name = dt.Rows[0]["Name"].ToString();
                   st.RollNumber = dt.Rows[0]["RollNumber"].ToString();
                   st.Password = dt.Rows[0]["Password"].ToString();
                   st.Gender = dt.Rows[0]["Gender"].ToString()[0];
                   st.Class = int.Parse(dt.Rows[0]["Class"].ToString());*/
                lblValidation.Visibility = Visibility.Visible;

            }
            else
            {
                st.Name = dt.Rows[0]["Name"].ToString();
                st.RollNumber = dt.Rows[0]["RollNumber"].ToString();
                st.Password = dt.Rows[0]["Password"].ToString();
                st.Gender = int.Parse(dt.Rows[0]["Gender"].ToString());
                st.Class = int.Parse(dt.Rows[0]["Class"].ToString());

                formInstructions form = new formInstructions(st, sessId);
                form.Show();
                this.Close();

            }
        }
    }
}
